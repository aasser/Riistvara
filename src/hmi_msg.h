#ifndef _HMI_MSG_H_
#define _HMI_MSG_H_
#include <avr/pgmspace.h>

#define STUDENT_NAME "Anneli Asser"
#define PROG_VER "Version: %S built on: %S %S"
#define LIB_VER "avr-libc version: %S avr-gcc version: %S"
#define MONTH_PROMPT "Enter Month name first letter >"
#define UPTIME_MSG "Current uptime: %lu s"

#define CLI_HELP_MSG "Implemented commands:"
#define CLI_NO_CMD "\n Command not implemented.\n Use <help> to get help.\n"
#define CLI_ARGS_MSG "\n To few or to many arguments for this command.\nUse <help>\n"

#define HELP_CMD "help"
#define HELP_HELP "Get help"

#define VER_CMD "version"
#define VER_HELP "Print FW version"

#define ASCII_CMD "ascii"
#define ASCII_HELP "print ASCII tables"

#define MONTH_CMD "month"
#define MONTH_HELP "Find matching month from lookup list. Usage: month <string>"


#define UART_STATUS_MASK 0xFF

#define READ_CMD "read"
#define READ_HELP "Read UID from RFID card"

#define ADD_CMD "add"
#define ADD_HELP "Add card to database. Usage: add <username>"

#define REMOVE_CMD "remove"
#define REMOVE_HELP "Remove card from database. Usage: remove <username>"

#define LIST_CMD "list"
#define LIST_HELP "List all valid cards"

#define ACCESS_DENIED_MSG "Access denied!"
#define NOT_ADDING_CARD_MSG1 "Found card \""
#define NOT_ADDING_CARD_MSG2 "\", not adding it again."
#define OUT_OF_MEMORY_MSG "Out of memory. Please remove cards."
#define NO_CARDS_ADDED_MSG "No cards added"
#define CARD_NOT_FOUND_MSG "Card not found"
#define LINKED_LIST_ERROR_MSG "Invalid situation when removing card"
#define UNABLE_TO_DETECT_CARD_MSG "Unable to detect card."
#define CARD_SELECTED_MSG "Card selected!"
#define UID_SIZE_MSG "UID size: 0x%02X"
#define UID_SAK_MSG "UID sak: 0x%02X"
#define CARD_UID_MSG "Card UID: "
#define CARD_NOT_SELECTED "Unable to select card.\n"

extern PGM_P const months[];


extern const char help_cmd[];
extern const char help_help[];
extern const char ver_cmd[];
extern const char ver_help[];
extern const char ascii_cmd[];
extern const char ascii_help[];
extern const char month_cmd[];
extern const char month_help[];

extern const char read_cmd[];
extern const char read_help[];
extern const char add_cmd[];
extern const char add_help[];
extern const char remove_cmd[];
extern const char remove_help[];
extern const char list_cmd[];
extern const char list_help[];

extern const char access_denied_msg[];

#endif /* _HMI_MSG_H_ */




